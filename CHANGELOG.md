0.3.1
=====

* fix: downgrade pre-commit to `2.17.0` because of some issues with python lib on pre-commit init

0.3.0
=====

* feat: pin pre-commit to 2.18.0
* maintenance: use the new TFlint docker image since old one is now deprecated
* maintenance: bump package dependencies

0.2.0
=====

* feat: install gcompat, needed for some hooks
* feat: allows the use of node
* refactor: install pre-commit as a pyz, to avoid the need of pip
* refactor: removes GCC at the end of installation
* fix: fixes shellcheck docker call

0.1.0
=====

* feat: removes dev build packages after installation
* feat: allows the use of shellcheck
* chore: updates pre-commit deps
* chore: updates dependencies

0.0.0
=====

* tech: initialise repository
* tech: docker pre-commit -> test OK
