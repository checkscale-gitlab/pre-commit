FROM registry.hub.docker.com/library/docker:dind

ENV BASH_VERSION=5.1.16-r0 \
    CACERTIFICATES_VERSION=20211220-r0 \
    GCC_VERSION=10.3.1_git20211027-r0 \
    GCOMPAT_VERSION=1.0.0-r4 \
    GIT_VERSION=2.34.1-r0 \
    MUSL_DEV_VERSION=1.2.2-r7 \
    PERL_VERSION=5.34.0-r1 \
    PRE_COMMIT_VERSION=2.17.0 \
    PYTHON3_VERSION=3.9.7-r4 \
    TERRAFORM_IMAGE="hashicorp/terraform" \
    TERRAFORM_IMAGE_VERSION=latest\
    TERRAFORM_DOCS_IMAGE="quay.io/terraform-docs/terraform-docs" \
    TERRAFORM_DOCS_IMAGE_VERSION=latest \
    TFLINT_IMAGE="ghcr.io/terraform-linters/tflint" \
    TFLINT_IMAGE_VERSION=latest \
    TFSEC_IMAGE="tfsec/tfsec" \
    TFSEC_IMAGE_VERSION=latest \
    SHELLCHECK_IMAGE="koalaman/shellcheck-alpine" \
    SHELLCHECK_VERSION=latest \
    NODE_IMAGE="library/node" \
    NODE_VERSION=latest

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

VOLUME /data

COPY ./resources /resources

RUN /resources/build && rm -rf /resources

WORKDIR /data
SHELL ["/bin/bash", "-c"]
ENTRYPOINT ["dockerd-entrypoint"]

LABEL "maintainer"="WildsBeavers" \
      "org.label-schema.base-image.name"="docker.io/library/alpine" \
      "org.label-schema.description"="pre-commit in alpine" \
      "org.label-schema.vendor"="Wildsbeavers" \
      "org.label-schema.applications.bash.version"=$BASH_VERSION \
      "org.label-schema.applications.terraform"=$TERRAFORM_IMAGE \
      "org.label-schema.applications.terraform.version"=$TERRAFORM_IMAGE_VERSION \
      "org.label-schema.applications.terraform-docs"=$TERRAFORM_DOCS_IMAGE \
      "org.label-schema.applications.terraform-docs.version"=$TERRAFORM_DOCS_IMAGE_VERSION \
      "org.label-schema.applications.tflint"=$TFLINT_IMAGE \
      "org.label-schema.applications.tflint.version"=$TFLINT_IMAGE_VERSION \
      "org.label-schema.applications.tfsec"=$TFSEC_IMAGE \
      "org.label-schema.applications.tfsec.version"=$TFSEC_IMAGE_VERSION \
      "org.label-schema.applications.shellcheck"=$SHELLCHECK_IMAGE \
      "org.label-schema.applications.shellcheck.version"=$SHELLCHECK_VERSION \
      "org.label-schema.applications.node"=$NODE_IMAGE \
      "org.label-schema.applications.node.version"=$NODE_VERSION \
      "org.label-schema.usage"="docker run -w /data -v $(pwd):/data --rm --privileged registry.gitlab.com/wild-beavers/docker/pre-commit:latest run -a --color=always"
