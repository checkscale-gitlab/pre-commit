# DinD Pre-commit

## Build it

```bash
docker build . -t <image>:<tag>
```

## How to Use it

```bash
docker run --privileged -v $(pwd):/data <image>:<tag>
```

or you can specify terraform version like this:

```bash
docker run --privileged -v $(pwd):/data -e TERRAFORM_IMAGE_VERSION="<version>" <image>:tag>
```

For the exact env variable you can change, please look at Dockerfile.

## Description

pre-commit DinD container image build files.

All hooks called by pre-commit will execute docker run commands inside a container.

Actually this hooks are pulling the latest tag for all officials images.

## Hooks which are already in it

- terraform  ( image pulled: `hashicorp/terraform` )
- terraform-docs ( image pulled: `quay.io/terraform-docs/terraform-docs`)
- tflint ( image pulled: `wata727/tflint` )
- tfsec( image pulled: `tfsec/tfsec`)

### If you need to add another hooks

You will have to:

- Add a new fake binary into resources folder like the others.(look at the list before). _Don't forget to make it executable_
- Add the ENV variable according to the image needed.
